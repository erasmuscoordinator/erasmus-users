package com.edu.agh.erasmus.users.model;

import lombok.Builder;
import lombok.Data;
import lombok.experimental.Wither;
import org.bson.types.ObjectId;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;

import java.util.List;

@Document
@Data
@Builder
@Wither
public class User { //todo divide this into student, coordinator, etc with accounts

    @Id private final ObjectId id;

    private final String email;
    private final String password;

    private final String firstName;
    private final String lastName;
    private final String university;
    private final String department;
    private final String faculty;
    private final StudiesDegree studiesDegree;

    //STUDENT ONLY
    private final String address;
    private final String phone;
    private final double gpa;
    private final List<Language> language;
    private final ErasmusParticipation erasmusParticipation;
    private final String completedSemester;
    private final boolean hasSocialScholarship;
    private final boolean hasPowerScholarship;
    private final boolean hasDisabilities;
    private final boolean isUserOnFirstSemesterSecondDegree;

    private final String indexNumber;
    private final String yearOfStudy;
    private final UniversityRole universityRole;

    public enum StudiesDegree {
        BACHELOR, MASTERS, SUPPLEMENTARY, POST_GRADUATE, DOCTORATE, NONE
    }

    public enum UniversityRole {
        STUDENT, COORDINATOR, ERASMUS_COORDINATOR, ADMINISTRATOR
    }
}