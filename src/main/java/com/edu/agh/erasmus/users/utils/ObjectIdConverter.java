package com.edu.agh.erasmus.users.utils;

import com.edu.agh.erasmus.users.exception.ErasmusUsersException;
import com.google.common.collect.ImmutableMap;
import org.bson.types.ObjectId;

import java.util.Map;

import static org.springframework.http.HttpStatus.BAD_REQUEST;

public final class ObjectIdConverter {
    public static ObjectId toObjectId(String id) {
        ObjectId objectId;
        try {
            objectId = new ObjectId(id);
        } catch (IllegalArgumentException e) {
            throw new ErasmusUsersException("MALFORMED ID", BAD_REQUEST);
        }
        return objectId;
    }

    public static Map<String, String> toMap(ObjectId id) {
        if (id == null) {
            return null;
        }
        return ImmutableMap.of("id", toStringId(id));
    }

    public static String toStringId(ObjectId objectId) {
        return objectId == null ? null : objectId.toString();
    }

}
