package com.edu.agh.erasmus.users.security;

import com.edu.agh.erasmus.users.model.UserRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.User;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;

import java.util.Collections;

@Service
@RequiredArgsConstructor
public class UserDetailsServiceImpl implements UserDetailsService {

    private final UserRepository userRepository;

    @Override
    public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {
        com.edu.agh.erasmus.users.model.User applicationUser = userRepository.findOneByEmail(username);
        if (applicationUser == null) {
            throw new UsernameNotFoundException(username);
        }
        SimpleGrantedAuthority role = new SimpleGrantedAuthority(applicationUser.getUniversityRole().toString());

        return new User(applicationUser.getEmail(), applicationUser.getPassword(), Collections.singletonList(role));
    }
}
