package com.edu.agh.erasmus.users.api.dto;

import com.edu.agh.erasmus.users.model.ErasmusParticipation;
import lombok.Builder;
import lombok.Data;

import static com.edu.agh.erasmus.users.utils.HtmlEncoder.htmlEncode;

@Data
@Builder
public class ErasmusParticipationGetDto {
    private final String yearOfStudy;
    private final String studiesDegree;
    private final String duration;

    public static ErasmusParticipationGetDto of(ErasmusParticipation participation) {
        if (participation == null) {
            return null;
        }
        return builder()
                .yearOfStudy(htmlEncode(participation.getYearOfStudy()))
                .studiesDegree(htmlEncode(participation.getStudiesDegree()))
                .duration(htmlEncode(participation.getDuration()))
                .build();
    }
}
