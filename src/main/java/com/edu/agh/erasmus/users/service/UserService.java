package com.edu.agh.erasmus.users.service;

import com.edu.agh.erasmus.users.api.dto.*;
import com.edu.agh.erasmus.users.exception.ErasmusUsersException;
import com.edu.agh.erasmus.users.mail.MailService;
import com.edu.agh.erasmus.users.model.User;
import com.edu.agh.erasmus.users.model.UserRepository;
import com.edu.agh.erasmus.users.model.UserResetPassword;
import com.edu.agh.erasmus.users.security.UserResetPasswordRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.security.access.prepost.PostAuthorize;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;
import java.util.Set;
import java.util.UUID;
import java.util.stream.Collectors;

import static com.edu.agh.erasmus.users.utils.ObjectIdConverter.toObjectId;
import static org.springframework.http.HttpStatus.CONFLICT;

@RequiredArgsConstructor
@Service
public class UserService {
    @Value("${erasmus.app.frontend.address}")
    private String frontendAddress;

    private static final String SAME_USER_OR_ADMIN = "hasAuthority('ADMINISTRATOR') || returnObject.email == authentication.name";
    private static final String RESET_PASSWORD_MSG_SUBJETCT = "Reset password request";

    private final UserRepository userRepository;
    private final UserResetPasswordRepository userResetPasswordRepository;
    private final BCryptPasswordEncoder passwordEncoder;
    private final MailService mailService;

    public UserGetDto registerUser(UserRegisterDto user) {
        validateUserCreation(user.getEmail());
        String encodedPassword = passwordEncoder.encode(user.getPassword());
        User savedUser = userRepository.save(user.asUser(encodedPassword));
        return UserGetDto.of(savedUser);
    }

    @PostAuthorize(SAME_USER_OR_ADMIN)
    public UserGetDto getUserById(String id) {
        User user = userRepository.findOneById(toObjectId(id));
        return UserGetDto.of(user);
    }

    @PostAuthorize(SAME_USER_OR_ADMIN)
    public UserGetDto deleteUserById(String id) {
        List<User> users = userRepository.deleteById(toObjectId(id));
        return extractFirstUser(users);
    }

    @PostAuthorize(SAME_USER_OR_ADMIN)
    public UserGetDto updateUser(UserUpdateDto updatedUser) {
        User currentUser = getUser(updatedUser.getId());
        User savedUser = userRepository.save(updatedUser.asUser(currentUser));
        return UserGetDto.of(savedUser);
    }

    public Set<UserGetDto> getUsers(String universityRole) {
        List<User> users = universityRole != null
                ? userRepository.findByUniversityRole(User.UniversityRole.valueOf(universityRole))
                : userRepository.findAll();
        return mapUsersToDto(users);
    }

    public UserGetDto changeUserRole(String userId, UserRoleDto userRole) {
        User currentUser = getUser(userId);
        User userWithNewRole = currentUser.withUniversityRole(userRole.asUniversityRole());
        User savedUser = userRepository.save(userWithNewRole);
        return UserGetDto.of(savedUser);
    }

    private Set<UserGetDto> mapUsersToDto(List<User> users) {
        return users.stream()
                .map(UserGetDto::of)
                .collect(Collectors.toSet());
    }

    private UserGetDto extractFirstUser(List<User> users) {
        if (users.isEmpty()) {
            return null;
        } else {
            return UserGetDto.of(users.get(0));
        }
    }

    private void validateUserCreation(String email) {
        if (userExists(email)) {
            throw new ErasmusUsersException("User with email: " + email + " already exists", CONFLICT);
        }
    }

    private boolean userExists(String email) {
        return userRepository.findOneByEmail(email) != null;
    }

    private User getUser(String id) {
        User currentUser = userRepository.findOneById(toObjectId(id));
        if (currentUser == null) {
            throw new ErasmusUsersException("User with id: " + id + " does not exists", CONFLICT);
        }
        return currentUser;
    }

    public void generateResetPasswordToken(UserPasswordResetDto passwordResetDto) {
        if (userExists(passwordResetDto.getEmail())) {
            String userEmail = passwordResetDto.getEmail();
            String resetPasswordToken = UUID.randomUUID().toString();
            userResetPasswordRepository.save(passwordResetDto.asUserResetPassword(resetPasswordToken));
            mailService.sendMessage(userEmail, RESET_PASSWORD_MSG_SUBJETCT, prepareResetPasswordEmailMessage(resetPasswordToken));
        }
    }

    private String prepareResetPasswordEmailMessage(String resetToken) {
        return "We received request for your password reset, if you want to continue please enter given url: " +
                frontendAddress + "/reset-password-confirm/" + resetToken;
    }

    public void resetPassword(UserPasswordResetConfirmDto passwordResetConfirmDto, String resetPasswordToken) {
        UserResetPassword userResetPasswordData = userResetPasswordRepository.findOneByResetPasswordToken(resetPasswordToken);
        if (userResetPasswordData != null) {
            User userToUpdatePassword = userRepository.findOneByEmail(userResetPasswordData.getEmail());
            userRepository.save(userToUpdatePassword.withPassword(passwordEncoder.encode(passwordResetConfirmDto.getNewPassword())));
            userResetPasswordRepository.delete(userResetPasswordData);
        }
    }
}
