package com.edu.agh.erasmus.users.model;

import lombok.Data;

@Data
public class ErasmusParticipation {
    private final String yearOfStudy;
    private final String studiesDegree;
    private final String duration;
}
