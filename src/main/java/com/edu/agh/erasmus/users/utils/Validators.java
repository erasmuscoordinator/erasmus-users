package com.edu.agh.erasmus.users.utils;

import com.edu.agh.erasmus.users.model.User.StudiesDegree;

public final class Validators {

    public static boolean validateFirstSemesterSecondDegree(String yearOfStudy, String studiesDegree) {
        return yearOfStudy != null && studiesDegree != null && yearOfStudy.equals("1") && studiesDegree.equals(StudiesDegree.MASTERS.toString());
    }

    public static StudiesDegree validateStudiesDegree(String studiesDegree) {
        return studiesDegree != null ? StudiesDegree.valueOf(studiesDegree) : StudiesDegree.NONE;
    }
}
