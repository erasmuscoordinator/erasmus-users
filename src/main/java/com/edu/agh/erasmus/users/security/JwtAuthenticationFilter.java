package com.edu.agh.erasmus.users.security;

import com.edu.agh.erasmus.users.api.dto.UserGetDto;
import com.edu.agh.erasmus.users.api.dto.UserLoginDto;
import com.edu.agh.erasmus.users.model.UserRepository;
import com.fasterxml.jackson.databind.ObjectMapper;
import io.jsonwebtoken.Jwts;
import io.jsonwebtoken.SignatureAlgorithm;
import lombok.RequiredArgsConstructor;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.AuthenticationException;
import org.springframework.security.core.userdetails.User;
import org.springframework.security.web.authentication.UsernamePasswordAuthenticationFilter;

import javax.servlet.FilterChain;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Date;

import static com.edu.agh.erasmus.users.security.SecurityProperties.HEADER_STRING;
import static com.edu.agh.erasmus.users.security.SecurityProperties.TOKEN_EXPIRATION_TIME;
import static com.edu.agh.erasmus.users.security.SecurityProperties.TOKEN_PREFIX;

@RequiredArgsConstructor
public class JwtAuthenticationFilter extends UsernamePasswordAuthenticationFilter {
    private final AuthenticationManager authenticationManager;
    private final UserRepository userRepository;
    private final String secret;

    @Override
    public Authentication attemptAuthentication(HttpServletRequest req,
                                                HttpServletResponse res) throws AuthenticationException {
        try {
            UserLoginDto userCredentials = new ObjectMapper()
                    .readValue(req.getInputStream(), UserLoginDto.class);

            return authenticationManager.authenticate(
                    new UsernamePasswordAuthenticationToken(
                            userCredentials.getEmail(),
                            userCredentials.getPassword(),
                            new ArrayList<>())
            );
        } catch (IOException e) {
            throw new RuntimeException(e);
        }
    }

    @Override
    protected void successfulAuthentication(HttpServletRequest req,
                                            HttpServletResponse res,
                                            FilterChain chain,
                                            Authentication auth) throws IOException, ServletException {

        User principal = (User) auth.getPrincipal();
        UserGetDto applicationUser = UserGetDto.of(userRepository.findOneByEmail(principal.getUsername()));

        String token = Jwts.builder()
                .setSubject(principal.getUsername())
                .claim("role", applicationUser.getUniversityRole())
                .setExpiration(new Date(System.currentTimeMillis() + TOKEN_EXPIRATION_TIME))
                .signWith(SignatureAlgorithm.HS512, secret.getBytes())
                .compact();
        res.addHeader(HEADER_STRING, TOKEN_PREFIX + token);
        res.addHeader("Content-Type", "application/json;charset=UTF-8");
        res.getWriter().write(new ObjectMapper().writeValueAsString(applicationUser));
    }
}
