package com.edu.agh.erasmus.users.api.dto;

import com.edu.agh.erasmus.users.model.UserResetPassword;
import lombok.Builder;
import lombok.Data;
import org.hibernate.validator.constraints.Email;
import org.hibernate.validator.constraints.NotBlank;

@Data
@Builder
public class UserPasswordResetDto {
    @Email
    @NotBlank
    private final String email;

    public UserResetPassword asUserResetPassword(String resetPasswordToken) {
        return UserResetPassword.builder()
                .resetPasswordToken(resetPasswordToken)
                .email(email)
                .build();
    }
}
