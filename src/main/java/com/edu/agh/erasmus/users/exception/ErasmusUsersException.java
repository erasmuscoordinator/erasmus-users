package com.edu.agh.erasmus.users.exception;

import com.fasterxml.jackson.annotation.JsonFormat;
import lombok.Getter;
import lombok.RequiredArgsConstructor;
import org.springframework.http.HttpStatus;

@JsonFormat(shape = JsonFormat.Shape.OBJECT)
@RequiredArgsConstructor
@Getter
public class ErasmusUsersException extends RuntimeException {
    private final String description;
    private final transient HttpStatus status;
}
