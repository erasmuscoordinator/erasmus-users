package com.edu.agh.erasmus.users.file;

import com.edu.agh.erasmus.users.exception.ErasmusUsersException;
import com.edu.agh.erasmus.users.model.Language;
import com.edu.agh.erasmus.users.model.User;
import com.edu.agh.erasmus.users.model.UserRepository;
import com.edu.agh.erasmus.users.utils.ImageValidator;
import lombok.RequiredArgsConstructor;
import org.bson.types.ObjectId;
import org.springframework.security.access.prepost.PostAuthorize;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;

import java.io.IOException;
import java.util.Map;

import static com.edu.agh.erasmus.users.utils.ObjectIdConverter.toMap;
import static com.edu.agh.erasmus.users.utils.ObjectIdConverter.toObjectId;
import static com.edu.agh.erasmus.users.utils.ObjectIdConverter.toStringId;
import static org.springframework.http.HttpStatus.BAD_REQUEST;

@Service
@RequiredArgsConstructor
class FileService {
    private final static String SAME_USER_OR_ADMIN = "hasAuthority('ADMINISTRATOR') || returnObject.email == authentication.name";
    private final FileRepository fileRepository;
    private final UserRepository userRepository;
    private final ImageValidator imageValidator;

    // FIXME Beware that languages are set via profile form on frontend
    // This will probably also fix problem with non-deleting old

    Map<String, String> saveFirstCertificate(MultipartFile multipartFile, String userId) throws IOException {
        User user = getUserInstance(userId); // returns cached object, not sure why...
        String originalFilename = multipartFile.getOriginalFilename();
        if (!imageValidator.hasImageExtension(originalFilename) || !imageValidator.isImage(multipartFile))
            throw new ErasmusUsersException("No proper file extension", BAD_REQUEST);
        ObjectId newCertificateFileId = fileRepository.saveFile(multipartFile.getInputStream(), originalFilename, multipartFile.getContentType());
        if (user.getLanguage().get(0) != null && user.getLanguage().get(0).getCertificateFileId() != null) {
            ObjectId oldCertificateFileId = toObjectId(user.getLanguage().get(0).getCertificateFileId());
            deleteOldCertificateFile(oldCertificateFileId);
        }
        Language updatedLanguage = user.getLanguage().get(0).withCertificateFileId(toStringId(newCertificateFileId)).withCertificateFileName(originalFilename);
        user.getLanguage().set(0, updatedLanguage);
        userRepository.save(user);
        return toMap(newCertificateFileId);
    }

    Map<String, String> saveSecondCertificate(MultipartFile multipartFile, String userId) throws IOException {
        User user = getUserInstance(userId); // returns cached object, not sure why...
        String originalFilename = multipartFile.getOriginalFilename();
        if (!imageValidator.hasImageExtension(originalFilename) || !imageValidator.isImage(multipartFile))
            throw new ErasmusUsersException("No proper file extension", BAD_REQUEST);
        if (user.getLanguage().get(1) != null && user.getLanguage().get(1).getCertificateFileId() != null) {
            ObjectId oldCertificateFileId = toObjectId(user.getLanguage().get(1).getCertificateFileId());
            deleteOldCertificateFile(oldCertificateFileId);
        }
        ObjectId newCertificateFileId = fileRepository.saveFile(multipartFile.getInputStream(), originalFilename, multipartFile.getContentType());
        Language updatedLanguage = user.getLanguage().get(1).withCertificateFileId(toStringId(newCertificateFileId)).withCertificateFileName(originalFilename);
        user.getLanguage().set(1, updatedLanguage);
        userRepository.save(user);
        return toMap(newCertificateFileId);
    }

    @PostAuthorize(SAME_USER_OR_ADMIN)
    private User getUserInstance(String userId) {
        return userRepository.findOneById(toObjectId(userId));
    }


    private void deleteOldCertificateFile(ObjectId id) {
        if (id != null && fileRepository.loadFileById(id).isPresent()) {
            fileRepository.deleteFileById(id);
        }
    }

}
