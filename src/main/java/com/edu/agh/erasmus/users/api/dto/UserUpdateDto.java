package com.edu.agh.erasmus.users.api.dto;

import com.edu.agh.erasmus.users.model.ErasmusParticipation;
import com.edu.agh.erasmus.users.model.Language;
import com.edu.agh.erasmus.users.model.User;
import com.edu.agh.erasmus.users.model.User.StudiesDegree;
import lombok.Builder;
import lombok.Data;

import java.util.List;

import static com.edu.agh.erasmus.users.utils.Validators.validateFirstSemesterSecondDegree;

@Data
@Builder
public class UserUpdateDto {
    private final String id;
    private final String firstName;
    private final String lastName;
    private final String university;
    private final String department;
    private final String faculty;
    private final String indexNumber;
    private final String yearOfStudy;
    private final String studiesDegree;
    private final String address;
    private final String phone;
    private final double gpa;
    private final List<Language> language;
    private final ErasmusParticipation erasmusParticipation;
    private final String completedSemester;
    private final boolean hasSocialScholarship;
    private final boolean hasPowerScholarship;
    private final boolean hasDisabilities;

    public User asUser(User currentUser) {
        return currentUser
                .withFirstName(firstName)
                .withLastName(lastName)
                .withUniversity(university)
                .withDepartment(department)
                .withFaculty(faculty)
                .withIndexNumber(indexNumber)
                .withYearOfStudy(yearOfStudy)
                .withStudiesDegree(StudiesDegree.valueOf(studiesDegree))
                .withAddress(address)
                .withPhone(phone)
                .withGpa(gpa)
                .withLanguage(language)
                .withErasmusParticipation(erasmusParticipation)
                .withCompletedSemester(completedSemester)
                .withHasSocialScholarship(hasSocialScholarship)
                .withHasPowerScholarship(hasPowerScholarship)
                .withHasDisabilities(hasDisabilities)
                .withUserOnFirstSemesterSecondDegree(validateFirstSemesterSecondDegree(yearOfStudy, studiesDegree));
    }
}
