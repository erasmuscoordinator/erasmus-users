package com.edu.agh.erasmus.users.security;

import com.edu.agh.erasmus.users.model.UserResetPassword;
import org.bson.types.ObjectId;
import org.springframework.data.mongodb.repository.MongoRepository;

public interface UserResetPasswordRepository extends MongoRepository<UserResetPassword, ObjectId> {
    UserResetPassword findOneByResetPasswordToken(String resetPasswordToken);

}
