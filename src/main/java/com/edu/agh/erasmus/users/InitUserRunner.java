package com.edu.agh.erasmus.users;

import com.edu.agh.erasmus.users.model.User;
import com.edu.agh.erasmus.users.model.UserRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Component;

import javax.annotation.PostConstruct;

import static com.edu.agh.erasmus.users.model.User.StudiesDegree.NONE;
import static com.edu.agh.erasmus.users.model.User.UniversityRole.ADMINISTRATOR;

@Component
@RequiredArgsConstructor
public class InitUserRunner {
    @Value("${erasmus.app.init.user.mail}")
    private String initAdminEmailAddress;
    @Value("${erasmus.app.init.user.password}")
    private String initAdminPassword;
    private final UserRepository userRepository;
    private final BCryptPasswordEncoder passwordEncoder;

    @PostConstruct
    public void init() {
        if (userRepository.findOneByEmail(initAdminEmailAddress) == null) {
            User initAdminUser = User.builder()
                    .email(initAdminEmailAddress)
                    .password(passwordEncoder.encode(initAdminPassword))
                    .studiesDegree(NONE)
                    .universityRole(ADMINISTRATOR)
                    .firstName("Erasmus")
                    .lastName("Admin")
                    .department("WIEiT")
                    .build();
            userRepository.save(initAdminUser);
        }
    }
}
