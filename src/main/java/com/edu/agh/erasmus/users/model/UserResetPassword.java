package com.edu.agh.erasmus.users.model;

import lombok.Builder;
import lombok.Data;
import lombok.experimental.Wither;
import org.hibernate.validator.constraints.Email;
import org.hibernate.validator.constraints.NotBlank;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;

@Document
@Data
@Builder
@Wither
public class UserResetPassword {
    @Id @NotBlank private final String resetPasswordToken;
    @Email @NotBlank private final String email;
}
