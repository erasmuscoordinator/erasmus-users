package com.edu.agh.erasmus.users.file;

import io.swagger.annotations.ApiOperation;
import lombok.RequiredArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import java.io.IOException;
import java.util.Map;

@RestController
@RequiredArgsConstructor
@RequestMapping("/api/users/{userId}/image/")
public class FileController {
    private final FileService fileService;

    @PostMapping("first_certificate")
    @ApiOperation("Saves first certificate file")
    public ResponseEntity<Map<String, String>> uploadFirstCertificate(@RequestParam("file") MultipartFile file,
                                                                      @PathVariable("userId") String userId) throws IOException {
        return new ResponseEntity<>(fileService.saveFirstCertificate(file, userId), HttpStatus.OK);
    }

    @PostMapping("second_certificate")
    @ApiOperation("Saves second certificate file")
    public ResponseEntity<Map<String, String>> uploadSecondCertificate(@RequestParam("file") MultipartFile file,
                                                                      @PathVariable("userId") String userId) throws IOException {
        return new ResponseEntity<>(fileService.saveSecondCertificate(file, userId), HttpStatus.OK);
    }

}
