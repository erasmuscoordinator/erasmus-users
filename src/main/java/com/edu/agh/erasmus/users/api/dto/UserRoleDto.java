package com.edu.agh.erasmus.users.api.dto;

import com.edu.agh.erasmus.users.model.User.UniversityRole;
import lombok.Builder;
import lombok.Data;

@Data
@Builder
public class UserRoleDto {
    private final String universityRole;

    public UniversityRole asUniversityRole() {
        return UniversityRole.valueOf(universityRole);
    }
}
