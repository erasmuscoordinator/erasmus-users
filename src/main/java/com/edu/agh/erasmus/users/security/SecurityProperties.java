package com.edu.agh.erasmus.users.security;

class SecurityProperties {
    static final long TOKEN_EXPIRATION_TIME = 3_600_000; // 1 hour
    static final String TOKEN_PREFIX = "Bearer ";
    static final String HEADER_STRING = "Authorization";
    static final String SIGN_UP_URL = "/api/users/";
    static final String RESET_PASSWORD_URL = "/api/users/password**";
    static final String RESET_PASSWORD_CONFIRM_URL = "/api/users/password/**";
}
