package com.edu.agh.erasmus.users.api.dto;

import lombok.Builder;
import lombok.Data;
import org.hibernate.validator.constraints.NotBlank;

@Data
@Builder
public class UserPasswordResetConfirmDto {
    @NotBlank private String newPassword;
}
