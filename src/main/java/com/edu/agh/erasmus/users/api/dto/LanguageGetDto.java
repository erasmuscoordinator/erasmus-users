package com.edu.agh.erasmus.users.api.dto;

import com.edu.agh.erasmus.users.model.Language;
import lombok.Builder;
import lombok.Data;

import java.util.Collection;
import java.util.List;
import java.util.stream.Collectors;

import static com.edu.agh.erasmus.users.utils.HtmlEncoder.htmlEncode;

@Data
@Builder
public class LanguageGetDto {
    private final String name;
    private final String examLevel;
    private final String grade;
    private final String certificateType;
    private final String certificateFileName;
    private final String certificateFileId;

    public static List<LanguageGetDto> of(Collection<Language> languages) {
        if (languages == null) {
            return null;
        }
        return languages.stream()
                .map(LanguageGetDto::of)
                .collect(Collectors.toList());
    }

    private static LanguageGetDto of(Language language) {
        return builder()
                .name(htmlEncode(language.getName()))
                .examLevel(htmlEncode(language.getExamLevel()))
                .grade(htmlEncode(language.getGrade()))
                .certificateType(htmlEncode(language.getCertificateType()))
                .certificateFileName(htmlEncode(language.getCertificateFileName()))
                .certificateFileId(language.getCertificateFileId())
                .build();
    }
}
