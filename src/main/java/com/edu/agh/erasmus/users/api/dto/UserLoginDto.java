package com.edu.agh.erasmus.users.api.dto;

import lombok.Builder;
import lombok.Data;

@Data
@Builder
public class UserLoginDto {
    private final String email;
    private final String password;
}
