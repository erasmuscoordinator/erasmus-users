package com.edu.agh.erasmus.users.api.dto;

import com.edu.agh.erasmus.users.model.User;
import lombok.Builder;
import lombok.Data;
import org.hibernate.validator.constraints.Email;
import org.hibernate.validator.constraints.NotBlank;

@Data
@Builder
public class UserRegisterDto {
    @Email @NotBlank private final String email;
    @NotBlank private final String password;
    @NotBlank private final String firstName;
    @NotBlank private final String lastName;

    public User asUser(String encodedPassword) {
        return User.builder()
                .email(email)
                .password(encodedPassword)
                .firstName(firstName)
                .lastName(lastName)
                .studiesDegree(User.StudiesDegree.NONE)
                .universityRole(User.UniversityRole.STUDENT)
                .build();
    }
}
