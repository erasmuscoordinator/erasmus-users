package com.edu.agh.erasmus.users.model;

import com.edu.agh.erasmus.users.model.User.UniversityRole;
import org.bson.types.ObjectId;
import org.springframework.data.mongodb.repository.MongoRepository;

import java.util.List;

public interface UserRepository extends MongoRepository<User, ObjectId> {
    User findOneById(ObjectId id);

    User findOneByEmail(String mail);

    List<User> findByUniversityRole(UniversityRole universityRole);

    List<User> deleteById(ObjectId id);
}