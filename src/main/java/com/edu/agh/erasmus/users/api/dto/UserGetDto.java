package com.edu.agh.erasmus.users.api.dto;

import com.edu.agh.erasmus.users.model.User;
import lombok.Builder;
import lombok.Data;

import java.util.List;

import static com.edu.agh.erasmus.users.utils.HtmlEncoder.htmlEncode;

@Data
@Builder
public class UserGetDto {
    private final String id;
    private final String email;
    private final String firstName;
    private final String lastName;
    private final String university;
    private final String department;
    private final String faculty;
    private final String indexNumber;
    private final String yearOfStudy;
    private final String studiesDegree;
    private final String universityRole;
    private final String address;
    private final String phone;
    private final double gpa;
    private final List<LanguageGetDto> language;
    private final ErasmusParticipationGetDto erasmusParticipation;
    private final String completedSemester;
    private final boolean hasSocialScholarship;
    private final boolean hasPowerScholarship;
    private final boolean hasDisabilities;
    private final boolean isUserOnFirstSemesterSecondDegree;

    public static UserGetDto of(User user) {
        return builder()
                .id(user.getId().toHexString())
                .email(htmlEncode(user.getEmail()))
                .firstName(htmlEncode(user.getFirstName()))
                .lastName(htmlEncode(user.getLastName()))
                .university(htmlEncode(user.getUniversity()))
                .department(htmlEncode(user.getDepartment()))
                .faculty(htmlEncode(user.getFaculty()))
                .indexNumber(htmlEncode(user.getIndexNumber()))
                .yearOfStudy(htmlEncode(user.getYearOfStudy()))
                .studiesDegree(String.valueOf(user.getStudiesDegree()))
                .universityRole(String.valueOf(user.getUniversityRole()))
                .address(htmlEncode(user.getAddress()))
                .phone(htmlEncode(user.getPhone()))
                .gpa((user.getGpa()))
                .language(LanguageGetDto.of(user.getLanguage()))
                .erasmusParticipation(ErasmusParticipationGetDto.of(user.getErasmusParticipation()))
                .completedSemester(htmlEncode(user.getCompletedSemester()))
                .hasSocialScholarship(user.isHasSocialScholarship())
                .hasPowerScholarship(user.isHasPowerScholarship())
                .hasDisabilities(user.isHasDisabilities())
                .isUserOnFirstSemesterSecondDegree(user.isUserOnFirstSemesterSecondDegree())
                .build();
    }
}
