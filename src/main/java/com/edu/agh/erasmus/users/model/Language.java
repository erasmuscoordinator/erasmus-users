package com.edu.agh.erasmus.users.model;

import lombok.Data;
import lombok.experimental.Wither;

@Data
@Wither
public class Language {
    private final String name;
    private final String examLevel;
    private final String grade;
    private final String certificateType;
    private final String certificateFileName;
    private final String certificateFileId;
}
