package com.edu.agh.erasmus.users.api;

import com.edu.agh.erasmus.users.api.dto.*;
import com.edu.agh.erasmus.users.service.UserService;
import io.swagger.annotations.ApiOperation;
import lombok.RequiredArgsConstructor;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import java.util.Collection;

@RestController
@RequiredArgsConstructor
@RequestMapping("/api/users/")
public class UserController {
    private final static String ONLY_ADMINISTRATOR = "hasAuthority('ADMINISTRATOR')";
    private final static Logger LOGGER = LoggerFactory.getLogger(UserController.class);
    private final UserService userService;

    @GetMapping("test")
    @ApiOperation(value = "Creates new user")
    ResponseEntity test() {
        LOGGER.info("Hello");
        return new ResponseEntity(HttpStatus.OK);
    }

    @PostMapping
    @ApiOperation(value = "Creates new user")
    ResponseEntity<UserGetDto> registerUser(@RequestBody @Validated UserRegisterDto user) {
        return new ResponseEntity<>(userService.registerUser(user), HttpStatus.CREATED);
    }

    @GetMapping
    @ApiOperation(value = "Gets all users")
    @PreAuthorize(ONLY_ADMINISTRATOR)
    ResponseEntity<Collection<UserGetDto>> getAllUsers(@RequestParam(value = "universityRole", required = false) String universityRole) {
        return new ResponseEntity<>(userService.getUsers(universityRole), HttpStatus.OK);
    }

    @GetMapping("{id}")
    @ApiOperation(value = "Gets user by id")
    ResponseEntity<UserGetDto> getUserById(@PathVariable String id) {
        return new ResponseEntity<>(userService.getUserById(id), HttpStatus.OK);
    }

    @PostMapping("password")
    @ApiOperation(value = "Generate reset password token and send url to perform change password action")
    ResponseEntity generateResetPasswordToken(@RequestBody @Validated UserPasswordResetDto passwordResetDto) {
        userService.generateResetPasswordToken(passwordResetDto);
        return new ResponseEntity(HttpStatus.OK);
    }

    @PostMapping("password/{token}")
    @ApiOperation(value = "Reset password with provided reset password token")
    ResponseEntity resetUserPassword(@RequestBody @Validated UserPasswordResetConfirmDto passwordResetConfirmDto, @PathVariable String token) {
        userService.resetPassword(passwordResetConfirmDto, token);
        return new ResponseEntity(HttpStatus.OK);
    }

    @PutMapping("{id}/role")
    @ApiOperation(value = "Change user role")
    @PreAuthorize(ONLY_ADMINISTRATOR)
    ResponseEntity<UserGetDto> changeUserRole(@PathVariable String id, @RequestBody UserRoleDto userRole) {
        return new ResponseEntity<>(userService.changeUserRole(id, userRole), HttpStatus.OK);
    }

    @PutMapping
    @ApiOperation(value = "Updates user info")
    ResponseEntity<UserGetDto> updateUser(@RequestBody UserUpdateDto user) {
        return new ResponseEntity<>(userService.updateUser(user), HttpStatus.OK);
    }

    @DeleteMapping("{id}")
    @ApiOperation(value = "Deletes user by id")
    ResponseEntity<UserGetDto> deleteUserById(@PathVariable String id) {
        UserGetDto user = userService.deleteUserById(id);
        if (user == null) {
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        } else {
            return new ResponseEntity<>(user, HttpStatus.OK);
        }
    }
}

